import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation'; // Version can be specified in package.json
import Login from './pages/login/Login';
import Home from './pages/home/Home';


const RootStack = createStackNavigator(
  {
    home: Home,
    login: Login,
  },
  {
    initialRouteName: 'login',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
