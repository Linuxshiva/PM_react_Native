import React from 'react';
import { Button, View, Text } from 'react-native';


export class Home extends React.Component {
    render() {
      const { navigation } = this.props;
      const email = navigation.getParam('email', 'NO-ID');
      const password = navigation.getParam('password', 'some default value');

        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>

              <Text>itemId: {JSON.stringify(email)}</Text>
        <Text>otherParam: {JSON.stringify(password)}</Text>
            
           
            <Button
              title="Go to login"
              onPress={() => this.props.navigation.navigate('login')}
            />
          </View>
        );
      }
}
export default Home;