import React from 'react';
import { Button, View, Text,Dimensions } from 'react-native';
import FloatingLabelInput from './FloatingLabelInput';


export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password:''
    };
  }

  handleTextChange = (newText) => this.setState({ value: newText });


  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Login Screen</Text>
          <View style={{ width:Dimensions.get('window').width,padding:20}}>
          <FloatingLabelInput
          label="Email"
          value={this.state.value}
          onChangeText={text=>this.setState({email:text})}
          />
        <FloatingLabelInput
          label="Password"
          value={this.state.value}
          onChangeText={text=>this.setState({password:text})}
        />
          </View>
          <Text>{this.state.email}+{this.state.password}</Text>
        
        <Button
              title="Go to Home"
              onPress={() => {
                /* 1. Navigate to the Details route with params */
                this.props.navigation.navigate('home', {
                  email: this.state.email,
                  password: this.state.password,
                });
              }}
              
        />
    
      </View>
    );
  }
}
export default Login;